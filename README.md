# OpenML dataset: kdd_internet_usage

https://www.openml.org/d/4133

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This data contains general demographic information on internet users in 1997.
Original Owner
[1]Graphics, Visualization, &amp; Usability Center College of Computing Geogia Institute of Technology Atlanta, GA
Donor [2]Dr Di Cook, Department of Statistics, Iowa State University
Date Donated: June 30, 1999
 
This data comes from a survey conducted by the Graphics and
Visualization Unit at Georgia Tech October 10 to November 16, 1997.

The particular subset of the survey provided here is the &quot;general demographics&quot; of internet users. The data have been recoded as entirely numeric, with an index to the codes described in the &quot;Coding&quot; file.

The full survey is available from the web site above, along with summaries, tables and graphs of their analyses. In addition there is information on other parts of the survey, including technology demographics and web commerce.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/4133) of an [OpenML dataset](https://www.openml.org/d/4133). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/4133/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/4133/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/4133/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

